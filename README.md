## Deploy Azure Landing Zones With Terraform

Authenticating to Azure using a Service Principal and a Client Secret (which is covered in this guide)

Ref: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret

[Deploy Azure Landing Zones With Terraform.](https://learn.microsoft.com/en-us/azure/cloud-adoption-framework/ready/landing-zone/deploy-landing-zones-with-terraform)
